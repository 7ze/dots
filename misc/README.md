# Miscellaneous Applications

Dotfiles of different applications, utility or otherwise

```custom
~
└── .config
    ├── python
    │   └── pythonstartup.py    python startup file
    ├── paru
    │   └── paru.conf           paru config file
    ├── neofetch
    │   ├── fetch.png           image for neofetch
    │   └── config.conf         configuration file for neofetch
    ├── htop
    │   └── htoprc              configuration file for htop
    ├── zathura
    │   └── zathurarc           configuration file for zathura
    ├── lf
    │   ├── icons               icons for lf 
    │   └── lfrc                configuration file for lf file manager
    ├── cava
    │   └── config              configuration file for cava music visualizer
    └── bat
        └── config              configuration file for bat
```
